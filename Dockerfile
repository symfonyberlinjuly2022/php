FROM php:8.1

# add env dependencies
RUN apt-get update && apt-get install --no-install-recommends -y \
    git git-lfs \
    zlib1g-dev libzip-dev unzip
RUN docker-php-ext-install zip

# add composer
ENV COMPOSER_HOME=/composer
COPY --from=composer:2.3.7 /usr/bin/composer /usr/bin/composer

# add pcov
RUN pecl install pcov-1.0.6
RUN docker-php-ext-enable pcov
